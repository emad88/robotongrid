package main;

import configuration.SpringConfiguration;
import grid.Grid;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by nikkhouy on 29.1.2016.
 */

public class ApplicationMain {


    private static Grid grid;
    private static int size;

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        grid = applicationContext.getBean("grid", Grid.class);
        size = 4;
        grid.initializeGrid(size);
        System.out.println("Total number of paths: " + grid.calculatePathNumber(0, 0, size - 1));
    }
}