package configuration;

import grid.Grid;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by nikkhouy on 2.2.2016.
 */
@Configuration
public class SpringConfiguration {


    @Bean(name = "grid")
    public Grid getGrid() {
        return new Grid();
    }
}
