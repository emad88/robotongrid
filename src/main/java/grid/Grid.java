package grid;

/**
 * Created by nikkhouy on 1.2.2016.
 */

public class Grid {

    private boolean[][] grid;
    private int total = 0;


    public void initializeGrid(int size) {
        grid = new boolean[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                grid[row][col] = false;
            }
        }
    }

    public int calculatePathNumber(int row, int col, int size) {
        total = 0;
        if (row == size && col == size) {
            return 1;
        }
        grid[row][col] = true;
        if (col < size && grid[row][col + 1] == false) {
            total += calculatePathNumber(row, col + 1, size);
        }
        if (col > 0 && grid[row][col - 1] == false) {
            total += calculatePathNumber(row, col - 1, size);
        }
        if (row < size && grid[row + 1][col] == false) {
            total += calculatePathNumber(row + 1, col, size);
        }
        if (row > 0 && grid[row - 1][col] == false) {
            total += calculatePathNumber(row - 1, col, size);
        }
        grid[row][col] = false;
        return total;
    }

}
